#! /usr/bin/python
"""
Testlink API Sample Python 3.x Client implementation
"""
import xmlrpc.client
import os
#import xmlrpc.server
import pdb
'''
# Framework imports
from config import *
from framework.utils import *
from framework.element_repository import ElementRepository
from framework.reporting import *
from framework.messages import msg_report
from framework.abstraction_layer import *
import framework.encryption as e'''
user = "Automation"
class TestlinkAPIClient:
    global report
    # substitute your server URL Here
    SERVER_URL = "http://testlink.catalina.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php"

    def __init__(self, devKey):
        self.server = xmlrpc.client.ServerProxy(self.SERVER_URL)
        self.devKey = devKey

    def reportTCResult(self, tcid, tpid, status):
        data = {"devKey":self.devKey, "tcid":tcid, "tpid":tpid, "status":status}
        return self.server.tl.reportTCResult(data)

    def createBuild(self, tpid, buildname, buildnotes):
        data = {"devKey":self.devKey, "testplanid":tpid, "buildname":buildname, "buildnotes":buildnotes}
        return self.server.tl.createBuild(data)

    def getInfo(self):
        return self.server.tl.about()

    def getTestCaseIDByName(self, testcasename):
        data = {"devKey":self.devKey, "testcasename":testcasename}
        return self.server.tl.getTestCaseIDByName(data)

    def reportTCResult(self, testcaseid, testplanid, status, buildname, platformid):
        data = {"devKey":self.devKey, "testcaseid": testcaseid, "testplanid":testplanid, "status":status, "buildname":buildname, "platformid":platformid}
        return self.server.tl.reportTCResult(data)

    def uploadTestCaseAttachment(self, testcaseid, filename, filetype, content):
        data = {"devKey":self.devKey, "testcaseid": testcaseid, "filename": filename, "filetype": filetype, "content": content}
        return self.server.tl.uploadTestCaseAttachment(data)

    '''def getbuildidfrombuildname(self, testplanid, builddesc):
        data = {"devKey":"c34800724dd6c6bc1e1061b4b1e7786c", "testplanid":testplanid}
        lsbuilds = self.server.tl.getBuildsforTestPlan
        builds_splt = str(lsbuilds[0]).split(",")
        for j in builds_split:
            print (j)
            if j.find(buildsdesc) == 0'''

    def addtestcasetotestplan(self, testprojectid, testplanid, tcexternalid, platformid):
        data = {"devKey":self.devKey, "testprojectid":testprojectid, "testplanid":testplanid, "testcaseexternalid": tcexternalid, "version": 1,"platformid":platformid}
        #pdb.set_trace()
        return self.server.tl.addTestCaseToTestPlan(data)

    def assignTestCaseExecutionTask(self,testcaseid,testprojectid,projectid,buildname,user,platformid):
        data={"devKey":self.devKey,"testprojectid":testprojectid,"testplanid":testplanid,"testcaseid":testcaseid,"buildname":buildname,"user":"Automation","platformid":platformid}
        return self.server.tl.assignTestCaseExecutionTask(data)

# substitute your Dev Key Here
# get info about the server
#print client.getInfo()
# Substitute for tcid and tpid that apply to your project
#result = client.createBuild(5, "API_0.0.0.5", "Build API_0.0.0.5 Notes")
#print (result)
def createbuild(params):
    #start_step("Create Build %s in Testlink" % buildname)
    #daily_build_version = "A - %s" %awards_version
    result = client.createBuild(params["testplanid"], params["buildname"], params["build notes"])
    #result = client.createBuild(5, "API_0.0.0.6", "Build API_0.0.0.6 Notes")
    #result = "[{'status': True, 'operation': 'createBuild', 'id': '12', 'message': 'Success!'}]"
    #result_split = str(result[0]).split(",")
    #pdb.set_trace()
    #for i in result_split:
        #print (i)
    if str(result[0]).find("Success")!= -1:
        print("success")
        #pass_step("[createbuild] The testlink build %s was created"% daily_build_version)
    elif str(result[0]).find("already exists") != -1:
        print("success")
        #pass_step("[createbuild] The testlink build %s already created"% daily_build_version)
    else:
        print("failure")
        #fail_step("[createbuild] The testlink build was not created")

def updateresult(testcaseid, testplanid, status, buildname, platformid):
    #start_step("Update result in Testlink")
    #pdb.set_trace()
    #tcstatus = report.get_overall_status()
    if status == "Passed":
        status = "p"
    elif status == "Failed":
        status = "f"
    elif status == "Blocked":
        status = "b"

    tcstatus = client.reportTCResult(testcaseid, testplanid, status, buildname, platformid)
    if str(tcstatus[0]).find("Success")!= -1:
        print("Passed")

def assigntestcaseuser(testcaseid,testplanid,testprojectid,user,platformid):
    #start_action("Assign %s to testcases "%user)
    client.assignTestCaseExecutionTask(testcaseid,testplanid,testprojectid,buildname,user,platformid)

client = TestlinkAPIClient("dbf4613faf25823935389e627708cbac")
client = TestlinkAPIClient("1373cdfcdd9afcbbd141dfb238d850e0")
'''def update_testlinkresult(params):
    status = client.reportTCResult(params["testcaseid"], params["testplanid"], params["teststatus"], params["buildid"], params["platformid"])
'''
def addtestcasetotestplan(params):
    #start_step("Associate %s to a test plan"%params["tcexternalid"])
    #pdb.set_trace()
    client.addtestcasetotestplan(params["testprojectid"], params["testplanid"], params["tcexternalid"], params["platformid"])
	
#===========================================================================================================================================
#update the result in TestLink 
fh=open(r"c:\testfile.txt","r")
testlink_data=fh.readlines()
for line in testlink_data:
	testcaseid = line.split(",")[0]
	status = line.split(",")[1]
	platform = line.split(",")[2]
testplanid = "88501"
print(status)
#pdb.set_trace()
buildname = "16.4.11.3"
if platform =="UAT":
	platformid ="38"
else:
	platformid = "37"

updateresult(testcaseid, testplanid, status, buildname, platformid)