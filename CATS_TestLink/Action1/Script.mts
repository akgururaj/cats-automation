﻿		
' ==============================================================================
' CATS - Contract Administration Transaction processing System (CATS)
' ==============================================================================

' Description: Main controller for CATS test cases flow.
' ==============================================================================
' Global Parameters:
' Parameter Name						Description (optional)
' --
' ==============================================================================
' Changelog:
' Date			Author				Description
' 04/20/2016	Anusuya				Initial Version
' 05/21/2016	Santosh				Main controller changes were made.
'                                   Seperated the action classes, as per the test data sheet.
' 02/12/2016    Santosh & Anusuya   Test Link Integration and ALM-Decouple.
' ==============================================================================

Option Explicit

'Variables declaration
Dim str_sheetName, str_actionName, int_actionRow, int_mainController, str_testName, bool_run,int_Count

Dim str_testCaseId,str_response, str_responseStatus,str_Round,str_TestLinkid,WSHshell,BuildFlag

'using default timeout to working fine with ".Select"
Setting("DefaultTimeout") = 30
Environment("debug_exit_on_fail")	= "Y"
'Set the current tab
str_sheetName = "Main Controller"
'Start Global - Action Index
Environment("int_actionRow") = 1
int_Count=cint(Environment("TestIteration"))
DataTable.GetSheet("Global").SetCurrentRow int_Count
bool_run = false
debugBeforeStart
StartIteration
Environment("BuildFlag") = "YES"
'Msgbox "TestIteration:::"&int_Count
'Msgbox "Sheet Name :::"&str_sheetName

If Ucase(DataTable("Run_Flag","Global")) = "Y" Then
	'Msgbox "Inside If condition"
	'Msg comment - debug - test iteration
	Environment("TestPlan_id") =DataTable("TestPlan_Id","Global")
	Environment("BuildName") =DataTable("Build_Name","Global")
    Environment("Build_Notes") =DataTable("Build_Notes","Global")
    
	debugMsgbox "test: " & DataTable("Test_Iteration","Global") & " - " & DataTable("Scenario_Description","Global")
	'------------------------------
	'GLOBAL SETUP for all Actions
	'
'	'To setup Actions
'	wsSetupActions

	'Go trhu the Main Controller sheet
	For int_mainController = 1 to DataTable.GetSheet(str_sheetName).GetRowCount
		DataTable.GetSheet(str_sheetName).SetCurrentRow int_mainController
		'Select Action Name
		str_actionName = DataTable("ActionName", str_sheetName)
		'Select Test Case Id
		str_testCaseId = DataTable("Test_Case_ID", str_sheetName)
		'Check Scenarios to Run
		
		
		
		'Msgbox DataTable("Country_Env","Global")
		'Msgbox Ucase(Trim(DataTable.GetSheet("Main Controller").GetParameter(DataTable("Country_Env","Global"))))
		
		If Ucase(Trim(DataTable.GetSheet("Main Controller").GetParameter(DataTable("Country_Env","Global")))) = "Y" Then
			'------------------------------
			'SETUP per Action
			'Start Action
			'StartAction DataTable("Scenario_Description","Global") &" - "& str_actionName
			'2015-06-15
			
			StartAction DataTable("Scenario_Description","Global") &" - "& DataTable("ActionDescription", str_sheetName)
			
			'Msg comment - debug - Action
			debugMsgbox DataTable("Scenario_Description","Global") &" - "& DataTable("ActionDescription", str_sheetName)

			'Start Action Index, before loop.
			Environment("int_actionRow") = 1

			'Go trhu the each Scenario selected sheet
			For int_actionRow = Cint(Environment("int_actionRow")) to DataTable.GetSheet(str_actionName).GetRowCount

				DataTable.GetSheet(str_actionName).SetCurrentRow Cint(Environment("int_actionRow"))
				'Check steps to Run
				If Ucase(Trim(DataTable("Run_Flag", str_actionName))) = "Y" Then
					'------------------------------
					'SETUP per Step
			   	    'Check Test Case Id to Run
					If Ucase(Trim(DataTable("Test_Case_ID", str_actionName))) = Ucase(Trim(str_testCaseId)) Then
						Environment("TestLink_id") = DataTable("TestLink_Id", str_sheetName)
'						Environment("TestPlan_id") =DataTable("TestPlan_Id",str_sheetName)
'						Environment("BuildName") =DataTable("Build_Name",str_sheetName)
'						Environment("Build_Notes") =DataTable("Build_Notes",str_sheetName)
						
						'msgbox DataTable("Country_Env", "Global")
						
						'msgbox DataTable("Country_Env", str_actionName)
						'str_actionName="Global"
						If	instr(Ucase(Trim(DataTable("Country_Env", "Global"))), Ucase(Trim(DataTable("Country_Env", str_actionName)))) >0 Then
						
							''msgBox DataTable("Test_Case_ID", str_actionName) & DataTable("Client_Name", str_actionName) & " - - - " & int_actionRow
							Dim str_messageTest
						
							str_messageTest = DataTable("Test_Case_ID", str_actionName) & ", " & DataTable.GetSheet(str_actionName).GetParameter(3).Name & ": " & DataTable(3, str_actionName) & ", ActionRow: " & int_actionRow

							StartStep "(start step) Test ID: " & str_messageTest
							'Msg comment - debug - Step
							debugmsgbox "(start step) Test ID: " & str_messageTest
						End if 
						
					   'Msgbox "Action name:"&str_actionName
						Select Case Trim(UCase(str_actionName))
						Case UCase("App_CATS_Login")
								App_CATS_Login
								
						Case UCase("App_CATS_001")
								App_CATS_001
						Case UCase("App_CATS_002")
								App_CATS_002
						Case UCase("App_CATS_003")
								App_CATS_003
						Case UCase("App_CATS_004")
								App_CATS_004
						Case UCase("App_CATS_005")
								App_CATS_005
						Case UCase("App_CATS_006")
								App_CATS_006
						Case UCase("App_CATS_007")
								App_CATS_007
						Case UCase("App_CATS_008")
								App_CATS_008
						Case UCase("App_CATS_009")
								App_CATS_009
						Case UCase("App_CATS_010")
								App_CATS_010
						Case UCase("App_CATS_011")
								App_CATS_011
						Case UCase("App_CATS_012")
								App_CATS_012
						Case UCase("App_CATS_013")
								App_CATS_013
						Case UCase("App_CATS_014")
								App_CATS_014
						Case UCase("App_CATS_015")
								App_CATS_015
						Case UCase("App_CATS_016")
								App_CATS_016
						Case UCase("App_CATS_017")
								App_CATS_017
						Case UCase("App_CATS_018")
								App_CATS_018
						Case UCase("App_CATS_019")
								App_CATS_019
						Case UCase("App_CATS_020")
								App_CATS_020

						Case UCase("App_CATS_021")
								App_CATS_021
						Case UCase("App_CATS_022")
								App_CATS_022
						Case UCase("App_CATS_023")
								App_CATS_023
						Case UCase("App_CATS_024")
								App_CATS_024
						Case UCase("App_CATS_025")
								App_CATS_025
						Case UCase("App_CATS_026")
								App_CATS_026
						Case UCase("App_CATS_027")
								App_CATS_027
						Case UCase("App_CATS_028")
								App_CATS_028
						Case UCase("App_CATS_029")
								App_CATS_029
						Case UCase("App_CATS_030")
								App_CATS_030
						Case UCase("App_CATS_031")
								App_CATS_031
						Case UCase("App_CATS_032")
								App_CATS_032
								
						Case Else
							str_messageTest = "Need to Setup - Trim(UCase(str_actionName)) and ADD the option/str_actionName: " & str_actionName
							'msgBox str_messageTest
							
							'Err.Raise 6  ' Raise an overflow error.
							Environment("LogErrorNumber") = "-6"
							'Report error and exit Test.
							debugExceptionExitTest str_messageTest 
								
						End Select
					
					'Start variable Test Name
'					str_testName = DataTable("Test_Name", str_actionName)
'					'Replace Variables from Global in Test Name, for example Cardss
'					webserviceReplaceString str_testName, "Test_Name", str_actionName
'
'					'Report if Test_Name is not empty.
'					If Trim(str_testName) <> "" Then
'						'Start Step / Test Name
'						StartStep str_testName
'					End If
'
'					'Select webservice Method and Call the respective function
'					webserviceSelectMethod str_response, str_responseStatus, str_actionName
'
'					'To Extract URL for Login Iphone Business Rules
'					wsMobileExtractLoginIphone str_response, str_responseStatus, str_actionName
'
'					'To check Response Assertions
'					webserviceResponseAssertion str_response, str_responseStatus, str_actionName
'
'					'To check Multiple Response Assertions
'					webserviceRespMultipleAssertion str_response, str_responseStatus, str_actionName

					End If
		
				End If
				'------------------------------
				'end SETUP per Step
				'
				'Set loop variable with global (before add, because loop add 1 in the begin)
				int_actionRow = Cint(Environment("int_actionRow"))
				'Set Environment Next Loop Step
				Environment("int_actionRow") = Cint(Environment("int_actionRow")) + 1
			Next
		End If
		'------------------------------
		'end SETUP per Action
        '
		EndAction
		
	Next
	'------------------------------
	'end GLOBAL SETUP for all Actions
	'
	bool_run = true
End If

' Finalizes the test
If bool_run Then
	FinishTest
	
	CloseAllWindow()
	
End If


