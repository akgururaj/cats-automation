'Execute vb script to make this current script work in 64bit machine
'Set WshShell = WScript.CreateObject("WScript.Shell") 
'WshShell.Run "I:\SQA\shared\Automation\Projects\CATS\CATS_Schuduler.vbs"

'Parameters

'Enter the URL to QC server
strQCURL = "http://orlpalmv01:8080/qcbin/"

'Enter Domain to use on QC server
strQCDomain = "AUTOMATION"

'Enter Project Name
strQCProject = "Automation_Poc"

'Enter the User name
strQCUser = "anagaraj"

'Enter user password 
strQCPassword = "abc123"

'Enter the path to the Test set folder
strTestSetFolderPath = "Root\CATS_Demo\CATS_US"

'Enter the test set to be run
strTestSetName = "CATS_SQA_Regression Test Suite"

'flag
disconnectFlag = 0

'Connect to ALM
set tdc = createobject("TDApiOle80.TDConnection")
tdc.InitConnectionEx strQCURL
tdc.login strQCUser, strQCPassword
tdc.Connect strQCDomain, strQCProject

'set the test set to be executed and get the required details
Set objShell = CreateObject("WScript.Shell")
Set TSetFact = tdc.TestSetFactory
Set tsTreeMgr = tdc.TestSetTreeManager
Set tsFolder = tsTreeMgr.NodeByPath(strTestSetFolderPath)

'split the multiple test set name and do the following for each of them
TestTestToExecute = split(strTestSetName,";")

for test = 0 to Ubound(TestTestToExecute)
'	msgbox TestTestToExecute(test)
	Set tsList = tsFolder.FindTestSets(TestTestToExecute(test))
 
	'set the test set to be executed and machine where it need to executed (here it will run locally)
	Set theTestSet = tsList.Item(1)
	Set Scheduler = theTestSet.StartExecution("")
	Scheduler.RunAllLocally = True
	Scheduler.run
	RunFinished = False
	Set execStatus = Scheduler.ExecutionStatus

	Do While RunFinished = False
		execStatus.RefreshExecStatusInfo "all", True
		RunFinished = execStatus.Finished
		Set EventsList = execStatus.EventsList

		For Each ExecEventInfoObj in EventsList
			strNowEvent = ExecEventInfoObj.EventType
		Next

		For i= 1 to execstatus.count
			Set TestExecStatusobj =execstatus.Item(i)
			intTestid = TestExecStatusobj.TestInstance
		Next
		
	Loop
	'disconnectFlag = test
Next

'msgbox disconnectFlag

'Disconnect from ALM
'if disconnectFlag = 1 then	
	'msgbox "About to discconnedt the projct"
	'tdc.DisconnectProject
'End if